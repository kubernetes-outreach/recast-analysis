## Recast Analysis on Federated Kubernetes

This demo does the following:
* taken an existing federated kubernetes control plane
* add two clusters to it
* launch a recast job to create a large workload (the more parallel jobs, the
  better)
* add a couple additional clusters to the federation while the job is running,
  external too

### Requirements

This demo relies on 5 clusters, with the following context names:
* atlas-recast-x
* atlas-recast-y
* atlas-recast-z
* tsystems
* gke

### Running the demo

* On a first terminal, $ tmux (ctrl-b D)
This will include a watch on jobs on the first two clusters, and a session to
run the demo. Start it with ./demo.sh.

* On a second terminal, $ tmux (ctrl-b E)
This monitors jobs on the remaining clusters.
