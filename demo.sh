#!/bin/bash
. ~/ws/recast-analysis/demo-magic.sh

. env.sh

clear
export DEMO_PROMPT="\e[33m\$\e[39m\$ \e[m "
export GOOGLE_APPLICATION_CREDENTIALS=gke-creds
export TYPE_SPEED=40

p "kubectl config get-contexts"
kubectl config get-contexts | grep -v ' fed '

for cluster in atlas-recast-x atlas-recast-y atlas-recast-z gke tsystems; do
   kubefed unjoin --context fed --host-cluster-context atlas-recast-x --cluster-context $cluster $cluster >> /dev/null 2>&1 || true
   kubectl --context $cluster -n federation-system delete serviceaccount ${cluster}-atlas-recast-x  >> /dev/null 2>&1 || true
   kubectl --context $cluster delete clusterrole federation-controller-manager:fed-${cluster}-atlas-recast-x >> /dev/null 2>&1 || true
   kubectl --context $cluster delete clusterrolebinding federation-controller-manager:fed-${cluster}-atlas-recast-x >> /dev/null 2>&1 || true
done

pe "openstack coe cluster list -c uuid -c name -c node_count -c master_count -c status"

p "# openstack coe federation create fed --hostcluster atlas-recast-x"

pe "kubectl --context fed get cluster"

p "# openstack coe federation join fed atlas-recast-x"

pe "kubefed join --context fed --host-cluster-context atlas-recast-x --cluster-context atlas-recast-x atlas-recast-x"

pe "kubefed join --context fed --host-cluster-context atlas-recast-x --cluster-context atlas-recast-y atlas-recast-y"

pe "kubectl --context fed get cluster"

pe "kubefed join --context fed --host-cluster-context atlas-recast-x --cluster-context atlas-recast-z atlas-recast-z"

pe "kubectl --context fed get cluster"

pe "kubefed join --context fed --host-cluster-context atlas-recast-x --cluster-context tsystems tsystems"

pe "kubefed join --context fed --host-cluster-context atlas-recast-x --cluster-context gke gke"

pe "kubectl --context fed get cluster"

sleep 1000

# watch
# watch -t -n 1 "echo federation; kubectl --context fed get job | grep -v pi | grep -v wflow-job-0bf2ee91-0387-4e6b-a357-f2dc309aa9da | grep -v wflow-job-9a49ac80-6792-46e6-9782-109ab0bd151f"
# watch -t -n 1 "echo atlas-recast-y; kubectl --context atlas-recast-y get job"

# cleanup
#
# kubectl --context atlas-recast-x get pod | grep logging  | awk '{print $1}' | xargs kubectl --context atlas-recast-x delete pod 
# kubectl --context atlas-recast-x -n kube-system get pod | grep filebeat  | awk '{print $1}' | xargs kubectl --context atlas-recast-x -n kube-system delete pod 
# for cluster in atlas-recast-x atlas-recast-y atlas-recast-z; do
#     kubectl --context ${cluster} get job | grep wflow | awk '{print $1}' | xargs kubectl --context ${cluster} delete job
# done

# tunnel
# ssh -L 8888:atlas-recast-x-ibe5fq5xynhp-minion-0.cern.ch:32453 rbritoda@lxplus-cloud.cern.ch 
